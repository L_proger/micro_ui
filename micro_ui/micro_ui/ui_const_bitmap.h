#ifndef ui_const_bitmap_h__
#define ui_const_bitmap_h__

#include "ui_element_base.h"

namespace micro_ui {
	class ui_const_bitmap : public ui_element_base {
	public:
		void set_bitmap(const color16* pixels, ui_size bitmap_size)
		{
			_pixels = pixels;
			_bitmap_size = bitmap_size;
		}

		virtual bool render_color(ui_point local_pos, color16& color) override {
			if((local_pos.x >= _bitmap_size.x) || (local_pos.y >= _bitmap_size.y) || local_pos.x < 0 || local_pos.y < 0) {
				return false;
			}
			color = _pixels[local_pos.y * _bitmap_size.x + local_pos.x];
			return true;
		}

	private:
		ui_size _bitmap_size;
		const color16* _pixels;
	};
}
#endif // ui_const_bitmap_h__
