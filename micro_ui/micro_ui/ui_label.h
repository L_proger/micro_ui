#ifndef ui_label_h__
#define ui_label_h__

#include "ui_element_base.h"
#include "ui_font.h"
#include <string.h>

namespace micro_ui {
	class ui_label : public ui_element_base {
	public:
		ui_font* font;
		uint8_t font_size;
		color16 font_color;

		ui_label() :_text(nullptr), font(nullptr), font_size(1), font_color(color24::white().as_color16()){

		}

		bool render_color(ui_point local_pos, color16& color) override {
			if ((font == nullptr) || (_text == nullptr) || (local_pos.y >= (ui_font::char_height() * font_size)) || (local_pos.y < 0)) {
				return false;
			}

			auto char_id = (local_pos.x / font_size) / font->char_width();
			auto char_local_x = (local_pos.x / font_size) % font->char_width();

			if(char_id >= _text_len){
				return false;
			}

			if(font->get_pixel(_text[char_id], char_local_x, local_pos.y / font_size))
			{
				color = font_color;
				return true;
			}
			return false;
		}
		void set_text(const char* text){
			_text = text;
			if(_text != nullptr){
				_text_len = strlen(_text);
			}
		}
	private:
		uint8_t _text_len;
		const char* _text;
	};
}
#endif // ui_label_h__
