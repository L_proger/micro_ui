#ifndef ui_grid_h__
#define ui_grid_h__

#include "ui_element_base.h"

namespace micro_ui
{
	class ui_grid : public ui_element_base {
	public:
		virtual bool render_color(ui_point local_pos, color16& color) override {
			if((((local_pos.x % 20) == 0) || ((local_pos.y % 20) == 0)) && ((local_pos.x %2 == 0) && (local_pos.y % 2 == 0))) {
				color = color24::gray().as_color16();
			} else {
				return false;
				//color = background_color;
			}
			return true;
		}
	};
}
#endif // ui_grid_h__
