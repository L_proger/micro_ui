#ifndef ui_element_h__
#define ui_element_h__

#include "micro_ui_types.h"
#include "ui_transform_provider.h"

namespace micro_ui
{
	template<typename T, size_t MaxChildren>
	using ui_element = ui_transform_provider<T, MaxChildren>;
}
#endif // ui_element_h__
