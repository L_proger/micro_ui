#ifndef ui_element_base_h__
#define ui_element_base_h__

#include "micro_ui_types.h"

namespace micro_ui{
	class ui_element_base {
	public:
		ui_size size;
		ui_color background_color;

		ui_rect local_rect() const {
			return ui_rect(ui_point(0,0), size);
		}

		ui_point local_position;

		virtual ui_point position() const {
			if (_parent != nullptr) {
				return _parent->position() + local_position;
			} else {
				return local_position;
			}
		}

		virtual bool render_color(ui_point local_pos, color16& color)
		{
			color = background_color;
			return true;
		}

		virtual uint32_t get_children_count() = 0;
		virtual ui_element_base* get_child(uint32_t id) = 0;
		virtual uint32_t get_max_children() = 0;
		ui_element_base* get_parent() {
			return _parent;
		}
		virtual void set_parent(ui_element_base* parent) {
			auto prev_parent = get_parent();
			if (prev_parent == parent) {
				return;
			}

			if (prev_parent != nullptr) {
				prev_parent->remove_child(this);
			}

			_parent = parent;

			if (_parent != nullptr) {
				_parent->add_child(this);
			}
		}

		ui_point to_local_space(ui_point world_point) {
			return world_point - position();
		}

		ui_point to_world_space(ui_point local_point) {
			return local_point + position();
		}
		virtual bool get_child_id(ui_element_base* child, size_t& id) = 0;
	protected:
		ui_element_base* _parent;
		virtual bool add_child(ui_element_base* child) = 0;
		virtual bool remove_child(ui_element_base* child) = 0;
	};
}


#endif // ui_element_base_h__
