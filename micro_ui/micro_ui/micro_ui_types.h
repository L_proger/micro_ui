#ifndef micro_ui_types_h__
#define micro_ui_types_h__

#include "rectangle.h"
#include "color.h"
#include "vector2.h"
#include <cstdint>

namespace micro_ui {
	using coord_1d = int16_t;
	using ui_rect = rectangle<coord_1d>;
	using ui_point = vector2<coord_1d>;
	using ui_size = vector2<coord_1d>;
	using ui_color = color16;


}
#endif // micro_ui_types_h__
