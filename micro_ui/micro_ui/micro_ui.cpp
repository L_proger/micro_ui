#include "window.h"
#include "display.h"
#include "ui_panel.h"
#include "ui_display.h"
#include "uvector.h"
#include <iostream>
#include <vector>
#include <amp.h>
#include "ui_element.h"
#include <chrono>
#include "ui_font.h"
#include <type_traits>
#include "homespun_font.h"
#include <cstdio>
#include "ui_label.h"
#include "ui_grid.h"
#include "ui_const_bitmap.h"
#include "bmp_to_img.h"

using namespace micro_ui;

struct render_context {
	color16 color_buffer;
};

void render_ui(ui_element_base* element, ui_point point, color16& result) {
	if (element == nullptr) {
		return;
	}

	auto lsp = element->to_local_space(point);
	auto lsr = element->local_rect();

	if (lsr.contains(lsp)) {

		color16 col;
		if(element->render_color(lsp, col))
		{
			result = col;
		}

		auto children = element->get_children_count();
		for (decltype(children) i = 0; i < children; ++i) {
			auto child = element->get_child(i);
			render_ui(child, point, result);
		}
	}
}

int main() {
	display disp(ui_size(320, 240));

	disp.fill_rect(ui_rect(0, 0, 320, 240), [](ui_rect rect, ui_point point) {return color24::gray().as_color16(); });
	disp.fill_rect(ui_rect(10, 10, 100, 100), [](ui_rect rect, ui_point point) {return color24::red().as_color16(); });

	ui_element<ui_display, 10> ui_core;
	ui_core.size = ui_size(320, 240);
	ui_core.background_color = color24::black().as_color16();

	ui_element<ui_panel, 2> progress_bg;
	progress_bg.set_parent(&ui_core);
	progress_bg.background_color = color24::green().as_color16();
	progress_bg.size = ui_size(100, 20);
	progress_bg.local_position = ui_point(10, 10);

	ui_element<ui_panel, 2> progress_line;
	progress_line.set_parent(&progress_bg);
	progress_line.background_color = color24::red().as_color16();
	progress_line.size = ui_size(58, 18);
	progress_line.local_position = ui_point(1, 1);

	ui_font fnt(&font[0][0], std::extent<decltype(font), 0>::value, std::extent<decltype(font), 1>::value);

	ui_element<ui_label, 1> label;
	label.set_parent(&ui_core);
	label.set_text("Test 1!");
	label.local_position = ui_point(10, 80);
	label.size = ui_size(100, 30);
	label.font = &fnt;
	label.font_size = 1;
	label.font_color = color24::red().as_color16();

	ui_element<ui_label, 1> label2;
	label2.set_parent(&ui_core);
	label2.set_text("Test 2!");
	label2.local_position = ui_point(10, 120);
	label2.size = ui_size(100, 30);
	label2.font = &fnt;
	label2.font_size = 2;
	label2.font_color = color24::green().as_color16();

	ui_element<ui_label, 1> label3;
	label3.set_parent(&ui_core);
	label3.set_text("Test 4!");
	label3.local_position = ui_point(10, 160);
	label3.size = ui_size(200, 30);
	label3.font = &fnt;
	label3.font_size = 4;
	label3.font_color = color24::blue().as_color16();

	ui_element<ui_grid, 1> grid;
	grid.set_parent(&ui_core);
	grid.local_position = ui_point(160, 120);
	grid.size = ui_size(160, 120);

	auto img = bmp_to_img::load("C:\\Users\\Sergey\\Desktop\\ICObject.bmp");
	ui_element<ui_const_bitmap, 1> bitmap;
	bitmap.set_bitmap(&img.pixels[0], ui_size(img.width, img.height));
	bitmap.set_parent(&ui_core);
	bitmap.local_position = ui_point(160, 0);
	bitmap.size = ui_size(img.width, img.height);

	auto img2 = bmp_to_img::load("C:\\Users\\Sergey\\Desktop\\hotSpot_button.bmp");
	ui_element<ui_const_bitmap, 1> bitmap2;
	bitmap2.set_bitmap(&img2.pixels[0], ui_size(img.width, img.height));
	bitmap2.set_parent(&ui_core);
	bitmap2.local_position = ui_point(192, 32);
	bitmap2.size = ui_size(img.width, img.height);

	auto prev_time = std::chrono::high_resolution_clock::now();
	
	uint32_t frame_id = 0;

	while (true) {
		/*bitmap2.local_position.x += 1;
		if (bitmap2.local_position.x > 224) {
			bitmap2.local_position.x = 192;
		}*/
		disp.fill_rect(ui_rect(0, 0, ui_core.size.x, ui_core.size.y), 
			[&ui_core](ui_rect rect, ui_point point) {
			color16 color;
			render_ui(&ui_core, point, color);
			return color;
		});

		disp.update();
		Sleep(0);
		frame_id++;

		auto now_time = std::chrono::high_resolution_clock::now();
		auto delta = std::chrono::duration_cast<std::chrono::milliseconds>(now_time - prev_time);
		if(delta.count() >= 1000)
		{
			auto fps = frame_id * 1000 / delta.count();
			std::cout << "FPS: " << fps << std::endl;
			prev_time = now_time;
			frame_id = 0;
		}
	}


	return 0;
}

