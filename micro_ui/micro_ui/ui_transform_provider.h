#ifndef ui_transform_provider_h__
#define ui_transform_provider_h__

#include "ui_element_base.h"
#include <type_traits>
#include "uvector.h"

namespace micro_ui
{
	template<typename Base, uint32_t MaxChildren>
	class ui_transform_provider : public Base
	{
	public:
		using children_container = uvector<ui_element_base*, MaxChildren>;

		uint32_t get_children_count() override {
			return _children.size();
		}
		ui_element_base* get_child(uint32_t id) override {
			return _children[id];
		}
		uint32_t get_max_children() override {
			return MaxChildren;
		}

		bool get_child_id(ui_element_base* child, size_t& id) override {
			for (children_container::size_type i = 0; i < _children.size(); ++i) {
				if (_children[i] == child) {
					id = (size_t)i;
					return true;
				}
			}
			return false;
		}


	private:
		children_container _children;
		bool add_child(ui_element_base* child) override {
			if (child == nullptr || child == this) {
				return false;
			}
			return _children.push_pack((ui_element_base*)child);
		}
		bool remove_child(ui_element_base* child) override {

			auto val = std::find(_children.begin(), _children.end(), child);
			if(val == _children.end())
			{
				return false;
			}

			_children.fast_erase(val);
			return true;
		}
		
	};
}
#endif // ui_transform_provider_h__
