#ifndef color_h__
#define color_h__

#include <cstdint>

namespace micro_ui {

	struct color16 {
		union {
			uint16_t value;
			struct {
				uint16_t r : 5;
				uint16_t g : 6;
				uint16_t b : 5;
			};
		};

		color16 swab_rb() const
		{
			color16 result;
			result.r = b;
			result.b = r;
			result.g = g;
			return result;
		}
	};

	struct color24 {
		uint8_t r;
		uint8_t g;
		uint8_t b;

		color24()
			:r(0), g(0), b(0) {
		}

		color24(uint8_t _r, uint8_t _g, uint8_t _b)
			:r(_r), g(_g), b(_b) {
		}

		static color24 red() {
			return color24(0xff, 0, 0);
		}

		static color24 green() {
			return color24(0, 0xff, 0);
		}

		static color24 blue() {
			return color24(0, 0, 0xff);
		}

		static color24 yellow() {
			return color24(0xff, 0xff, 0);
		}

		static color24 white() {
			return color24(0xff, 0xff, 0xff);
		}

		static color24 black() {
			return color24(0, 0, 0);
		}


		static color24 gray() {
			return color24(0x80, 0x80, 0x80);
		}

		color16 as_color16() {
			color16 result;
			result.r = r >> 3;
			result.g = g >> 2;
			result.b = b >> 3;
			return result;
		}
	};

	struct color32{
		uint8_t r;
		uint8_t g;
		uint8_t b;
		uint8_t a;

		color32():r(0), g(0), b(0), a(0){
			
		}

		color32(uint8_t _r, uint8_t _g, uint8_t _b, uint8_t _a) :r(_r), g(_g), b(_b), a(_a) {

		}

		color32(color16 src)
		{
			r = src.r << 3;
			g = src.g << 2;
			b = src.b << 3;
			a = 0xff;
		}
	};
}

#endif // color_h__
